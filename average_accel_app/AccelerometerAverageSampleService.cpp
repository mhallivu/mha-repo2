#include "movesense.h"
#include "AccelerometerAverageSampleService.h"
#include "app-resources/resources.h"
#include "common/core/debug.h"
#include "meas_acc/resources.h"
#include "ui_ind/resources.h"
#include <float.h>

#define ACC_PATH_WITH_SAMPLERATE "Meas/Acc/13"

const char* const AccelerometerAverageSampleService::LAUNCHABLE_NAME = "AccAvSample";

static const wb::ExecutionContextId sExecutionContextId =
    WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI::EXECUTION_CONTEXT;

static const wb::LocalResourceId sProviderResources[] =
{
    WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI::LID,
};

AccelerometerAverageSampleService::AccelerometerAverageSampleService()
    : ResourceClient(WBDEBUG_NAME(__FUNCTION__), sExecutionContextId),
      ResourceProvider(WBDEBUG_NAME(__FUNCTION__), sExecutionContextId),
      LaunchableModule(LAUNCHABLE_NAME, sExecutionContextId),
      mOngoingRequests(),
      mMeasAccResourceId(wb::ID_INVALID_RESOURCE),
      isRunning(false),
      mSamplesIncluded(0),
      mMaxAccelerationSq(0.0f)
{
}

AccelerometerAverageSampleService::~AccelerometerAverageSampleService()
{
}

bool AccelerometerAverageSampleService::initModule()
{
    if (registerProviderResources(sProviderResources) != wb::HTTP_CODE_OK)
    {
        return false;
    }

    mModuleState = WB_RES::ModuleStateValues::INITIALIZED;
    return true;
}

void AccelerometerAverageSampleService::deinitModule()
{
    unregisterProviderResources(sProviderResources);
    mModuleState = WB_RES::ModuleStateValues::UNINITIALIZED;
}

bool AccelerometerAverageSampleService::startModule()
{
    mModuleState = WB_RES::ModuleStateValues::STARTED;
    return true;
}

void AccelerometerAverageSampleService::stopModule()
{
    stopRunning();
    mModuleState = WB_RES::ModuleStateValues::STOPPED;
}

void AccelerometerAverageSampleService::onSubscribeResult(wb::RequestId requestId,
                                                   wb::ResourceId resourceId,
                                                   wb::Result resultCode,
                                                   const wb::Value& rResultData)
{
    DEBUGLOG("AccelerometerAverageSampleService::onSubscribeResult() called. resourceId: %u, result: %u",
             resourceId.localResourceId, resultCode);

    wb::Request relatedIncomingRequest;
    bool relatedRequestFound = mOngoingRequests.get(requestId, relatedIncomingRequest);

    if (relatedRequestFound)
    {
        returnResult(relatedIncomingRequest, wb::HTTP_CODE_OK);
    }
}

wb::Result AccelerometerAverageSampleService::startRunning(wb::RequestId& remoteRequestId)
{
    if (isRunning)
    {
        return wb::HTTP_CODE_OK;
    }

    DEBUGLOG("AccelerometerAverageSampleService::startRunning()");

    // Reset average acceleration members
    averageValue = {0};
    mSamplesIncluded = 0;

    wb::Result result = getResource(ACC_PATH_WITH_SAMPLERATE, mMeasAccResourceId);
    if (!wb::RETURN_OKC(result))
    {
        return result;
    }

    result = asyncSubscribe(mMeasAccResourceId, AsyncRequestOptions(&remoteRequestId, 0, true));

    if (!wb::RETURN_OKC(result))
    {
        DEBUGLOG("asyncSubscribe threw error: %u", result);
        return wb::HTTP_CODE_BAD_REQUEST;
    }
    isRunning = true;

    return wb::HTTP_CODE_OK;
}

wb::Result AccelerometerAverageSampleService::stopRunning()
{
    if (!isRunning)
    {
        return wb::HTTP_CODE_OK;
    }

    if (isResourceSubscribed(WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI::ID) == wb::HTTP_CODE_OK)
    {
        DEBUGLOG("AccelerometerAverageSampleService::stopRunning() skipping. Subscribers still exist.");
        return wb::HTTP_CODE_OK;
    }

    DEBUGLOG("AccelerometerAverageSampleService::stopRunning()");

    // Unsubscribe the LinearAcceleration resource, when unsubscribe is done, we get callback
    wb::Result result = asyncUnsubscribe(mMeasAccResourceId, NULL);
    if (!wb::RETURN_OKC(result))
    {
        DEBUGLOG("asyncUnsubscribe threw error: %u", result);
    }
    isRunning = false;
    releaseResource(mMeasAccResourceId);

    return wb::HTTP_CODE_OK;
}

void AccelerometerAverageSampleService::onNotify(wb::ResourceId resourceId,
                                          const wb::Value& value,
                                          const wb::ParameterList& parameters)
{
    DEBUGLOG("onNotify() called.");

    // Confirm that it is the correct resource
    switch (resourceId.localResourceId)
    {
    case WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI::LID:
    {
        const WB_RES::AccData& linearAccelerationValue =
            value.convertTo<const WB_RES::AccData&>();

        if (linearAccelerationValue.arrayAcc.size() <= 0)
        {
            // No value, do nothing...
            return;
        }

        const wb::Array<wb::FloatVector3D>& arrayData = linearAccelerationValue.arrayAcc;

        uint32_t relativeTime = linearAccelerationValue.timestamp;
        averageValue = {0};
        mSamplesIncluded = arrayData.size();
        /* 1 second has 13 measurements - ignore rest */
        if (mSamplesIncluded >13)
            mSamplesIncluded= 13;
        for (size_t i = 0; i < mSamplesIncluded; i++)
        {
            wb::FloatVector3D accValue = arrayData[i];
            averageValue.mX += accValue.mX;
            averageValue.mY += accValue.mY;
            averageValue.mZ += accValue.mZ;

        }
        /* Average */
        averageValue.mX /= mSamplesIncluded;
        averageValue.mY /= mSamplesIncluded;
        averageValue.mZ /= mSamplesIncluded;
        averageValue.relativeTime = relativeTime;
        
        if (mSamplesIncluded > 0)
        {
            const WB_RES::VisualIndType type
                = WB_RES::VisualIndTypeValues::SHORT_VISUAL_INDICATION; // defined in ui/ind.yaml
            /* Average */
            averageValue.mX /= mSamplesIncluded;
            averageValue.mY /= mSamplesIncluded;
            averageValue.mZ /= mSamplesIncluded;
            
            // Notify our subscribers
            //averigrelativeTime = relativeTime;

            // Set LED
            asyncPut(WB_RES::LOCAL::UI_IND_VISUAL(), AsyncRequestOptions::Empty, type); // PUT request to trigger led blink
            // ignore the immediate and asynchronous Results as this is not critical
            
            mSamplesIncluded = 0;
            mMaxAccelerationSq = FLT_MIN;

            // and update our WB resource. This causes notification to be fired to our subscribers
            updateResource(WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI(), ResponseOptions::Empty, averageValue);
        }
        else
        {
            averageValue = {0};
        }
        break;
    }
    }
}

void AccelerometerAverageSampleService::onSubscribe(const wb::Request& request,
                                             const wb::ParameterList& parameters)
{
    switch (request.getResourceId().localResourceId)
    {
    case WB_RES::LOCAL::EXERCISE_SUMVECTOR_MATTI::LID:
    {
        // Someone subscribed to our service. Start collecting data and notifying when our service changes state (every 10 seconds)
        wb::RequestId remoteRequestId;
        wb::Result result = startRunning(remoteRequestId);

        if (isRunning)
        {
            returnResult(request, wb::HTTP_CODE_OK);
            return;
        }

        if (!wb::RETURN_OK(result))
        {
            returnResult(request, result);
            return;
        }
        bool queueResult = mOngoingRequests.put(remoteRequestId, request);
        (void)queueResult;
        WB_ASSERT(queueResult);
        break;
    }
    default:
        ASSERT(0); // Should not happen
    }
}

void AccelerometerAverageSampleService::onRemoteWhiteboardDisconnected(wb::WhiteboardId whiteboardId)
{
    DEBUGLOG("AccelerometerAverageSampleService::onRemoteWhiteboardDisconnected()");
    stopRunning();
}

void AccelerometerAverageSampleService::onClientUnavailable(wb::ClientId clientId)
{
    DEBUGLOG("AccelerometerAverageSampleService::onClientUnavailable()");
    stopRunning();
}
