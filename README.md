##General
This repository contains source code for average_accel_app. 

#Retrieval of movesense library 
In order to compile it
you need to retieve movesence-device-lib by executing command
 git clone https://bitbucket.org/suunto/movesense-device-lib.git



#Compilation
After that you can compile for simulation environment with commands
cmake -G "Visual Studio 15 2017" -DMOVESENSE_CORE_LIBRARY=../MovesenseCoreLib/ ../samples/average_accel_app
This works only on windows 10 environment even though Visual Studio is available for MAc and Ubuntu.

Compilation for target is done by commands:
cmake -G Ninja -DMOVESENSE_CORE_LIBRARY=../MovesenseCoreLib/ -DCMAKE_TOOLCHAIN_FILE=../MovesenseCoreLib/toolchain/gcc-nrf52.cmake ../samples/average_accel_app

ninja


As i don't have windows 10 lisence available actual simulation was not carried out.
The reason for the lack of Mac Os and Ubuntu support is that there is binary implementation of
the movesence-library only for windows and arm targets.

Linking for targets is problematic, because the provided arm version of core library depens on
very specific gcc-arm compiler. At least Ubuntu 16.04 and 18.04 don't work.  Same goes for Mac
arm-none-eabi-gcc (GNU Tools for Arm Embedded Processors 7-2017-q4-major) 7.2.1 20170904
(release) [ARM/embedded-7-branch revision 255204]

## License
As the exercise is based on lisenced material the same lisence applies to this exercise.
See [LICENSE.pdf](LICENSE.pdf) for details on Movesense license.
